import { Component, OnInit, Input } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MessageService } from 'primeng/api';
import { PersonaActualizacion, PersonaActualizacionModel } from 'src/models/persona';
import { LocalAPI } from 'src/services/local.service';
import { PersonaAPI } from 'src/services/persona.service';
import { fileHandler } from 'src/utils/file-handler';
import { checkPersona } from 'src/utils/object.checker';
import { showToast } from 'src/utils/toast';

@Component({
  selector: 'app-datos-personales',
  templateUrl: './datos-personales.component.html',
  styleUrls: ['./datos-personales.component.scss']
})
export class DatosPersonalesComponent implements OnInit {

  personaActualizacion: PersonaActualizacion = new PersonaActualizacion();
  personaModel: PersonaActualizacionModel = new PersonaActualizacionModel();

  constructor(private apiLocal: LocalAPI, private apiPersona: PersonaAPI, 
    private ngxService: NgxUiLoaderService, private messageService: MessageService) {
      
      apiPersona.personaActualizacion.subscribe(
        data => {
          this.personaActualizacion = data;
          this.personaModel = new PersonaActualizacionModel(this.personaActualizacion);
          this.inicialesPersona = `${this.personaModel.nombres.substr(0,1).toUpperCase()}${this.personaModel.apellidos.substr(0,1).toUpperCase()}`
          console.log(this.personaModel);
        }
      )

    }
  
  input;
  inicialesPersona = "";
  sexos = [
    {name: "Femenino", value: "F"},
    {name: "Masculino", value: "M"}
  ];
  user = JSON.parse(localStorage.getItem("currentUser"));
  estadosCiviles = [
    {name: "Soltero/a", value: "Soltero/a"},
    {name: "Casado/a", value: "Casado/a"},
    {name: "Divorciado/a", value: "Divorciado/a"},
    {name: "Viudo/a", value: "Viudo/a"}
  ];

  ngOnInit(): void {
    this.getNacionalidades();
    this.getOficinas();
    this.input = <HTMLInputElement> document.getElementById('profile-picker');
  }

  nacionalidades = [];
  getNacionalidades = ()=> {
    this.apiLocal.getNacionalidades().subscribe(
      data => {
        this.nacionalidades = data;
      }
    )
  }
  
  oficinas = [];
  getOficinas = ()=> {
    this.apiPersona.getOficinas().subscribe(
      data => {
        if(data.estado == 0){
          this.oficinas = data.dato;
        }
      }
    )
  }

  guardarDatos(){
    if(checkPersona(this.personaModel, null)){
      this.ngxService.start();
      console.log(this.personaModel);
      this.apiPersona.guardarDatos(this.personaModel, this.personaActualizacion.method).subscribe(
        data => {
          if(data["estado"] == 0){
            showToast(null,'Éxito', 'Datos guardados correctamente', this.messageService, 'success');
            this.actualizarDatosService();
          }

          
          this.ngxService.stop();
        },
        error => {
          showToast(null,'Error', 'Error al guardar datos', this.messageService, 'error');
        }
      )
    }else{
      showToast(null,'Error', 'Complete los campos requeridos (*)', this.messageService, 'error'); 
    }
  }

  guardarFoto = ()=> {
    this.apiPersona.guardarDatos(this.personaModel, this.personaActualizacion.method).subscribe(
      data =>{
        console.log(data);
        this.actualizarDatosService();
      },
      error => {}
    )
  }

  openFileSelector(){
    this.input.click();
  }

  onChangeFileSelector = (files:FileList) => {
    fileHandler(files).then((archivo) => {
      if(archivo != null){
        this.personaModel.foto = archivo.file;
        this.guardarFoto();
      }else{
        showToast(null,'Error en el archivo', 'Formato inválido o tamaño excedido', this.messageService, 'error');
      }
      this.input.value = "";
    });
  }

  eliminarFoto = () => {
    this.personaModel.foto = null;
    this.guardarFoto();
  }

  verFoto = false;

  actualizarDatosService(){
    this.personaActualizacion.fechaNacimiento = this.personaModel.fechaNacimiento;
    this.personaActualizacion.sexo = this.personaModel.sexo;
    this.personaActualizacion.estadoCivil = this.personaModel.estadoCivil;
    this.personaActualizacion.nacionalidad = this.personaModel.nacionalidad;
    this.personaActualizacion.email = this.personaModel.email;
    this.personaActualizacion.telefono = this.personaModel.telefono;
    this.personaActualizacion.oficina = this.personaModel.oficina;
    this.personaActualizacion.foto = this.personaModel.foto;
    if(this.personaActualizacion.method == "POST"){
      this.personaActualizacion.method = "PUT";
    }

    this.apiPersona.setPersonaActualizacion(this.personaActualizacion);
  }
}
