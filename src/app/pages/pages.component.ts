import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { AuthAPI } from 'src/services/auth.service';
import { NgxUiLoaderService, SPINNER } from 'ngx-ui-loader';
import { PersonaActualizacion } from 'src/models/persona';
import { PersonaAPI } from 'src/services/persona.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {

  loaderback = "rgba(31, 45, 64, 1)";
  constructor(private apiAuth: AuthAPI, private router: Router, private ngxService: NgxUiLoaderService, private apiPersona: PersonaAPI) {
    //let user = JSON.parse(localStorage.getItem("currentUser"));
    ngxService.start();
    apiPersona.getPersona().subscribe(
      data => {
        if(data.estado == 0) {
          this.setPersona(data.usuario);
        } else {
          this.router.navigateByUrl('/login');
        }
        ngxService.stop();
        this.loaderback = "rgba(31, 45, 64, 0.5)";
      }
    )

    this.apiPersona.personaActualizacion.subscribe(
      data => {
        this.personaActualizacion = {...data};

        if(this.items.length == 0){
          this.items = [
            {
              label: this.personaActualizacion?.usuario,
              icon: 'pi pi-user text-rosado',
              items: [
              {
                label: 'Perfil',
                icon: 'pi pi-user text-rosado',
                command: () => {
                  this.displayUserModal = true;
                }
              },
              {
                label: 'Cerrar Sesión',
                icon: 'pi pi-sign-out text-rosado',
                command: () => {
                  this.logout();
                }
              }
              ]
            }
          ];
          if(this.personaActualizacion.superior != null){
              this.sli = this.personaActualizacion.superior.toUpperCase().substr(0,1);   
          }
        }
        console.log(this.personaActualizacion);
      }
    )
  }

  SPINNER = SPINNER;
  items: MenuItem[] = [];
  personaActualizacion: PersonaActualizacion = new PersonaActualizacion();

  displayUserModal = false;

  sli = null;
  ngOnInit(): void {}

  logout = () => {
    this.ngxService.start();
    this.apiAuth.logout().subscribe(
      data => {
        this.ngxService.stop();
        this.router.navigateByUrl('/login');
      }
    )
  }

  setPersona(usuario){
    this.apiPersona.setPersonaActualizacion(usuario);
  }

}
