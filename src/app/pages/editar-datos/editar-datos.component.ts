import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/api';

@Component({
  selector: 'app-editar-datos',
  templateUrl: './editar-datos.component.html',
  styleUrls: ['./editar-datos.component.scss']
})
export class EditarDatosComponent implements OnInit {

  constructor() { }
  mensajes: Message[];
  ngOnInit(): void {
    this.mensajes = [
      {severity:'info', summary:'Actualización de datos', detail:'Por favor, completa tus datos de las siguientes secciones'}
    ];
  }

}
