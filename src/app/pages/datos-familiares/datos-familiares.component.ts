import { Component, Input, OnInit } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MessageService } from 'primeng/api';
import { EstructuraFamiliar, Familiar } from 'src/models/familiar';
import { Persona } from 'src/models/persona';
import { PersonaAPI } from 'src/services/persona.service';
import { showToast } from 'src/utils/toast';

@Component({
  selector: 'app-datos-familiares',
  templateUrl: './datos-familiares.component.html',
  styleUrls: ['./datos-familiares.component.scss']
})
export class DatosFamiliaresComponent implements OnInit {

  constructor(private ngxService: NgxUiLoaderService, private messageService: MessageService, private apiPersona: PersonaAPI) { }

  @Input() set p(obj: Persona){
    this.persona = obj;
    try {
      // this.familiares = JSON.parse(this.persona.familiares);
      if(this.familiares == null){
        this.familiares = new EstructuraFamiliar();
      }
    }catch(e){
      this.familiares = new EstructuraFamiliar();
    }
  }

  persona: Persona = new Persona();

  familiares = new EstructuraFamiliar();
  
  ngOnInit(): void {
    
  }
  
  agregarHijoModal = false;
  hijoModal: Familiar = new Familiar();
  agregarHijo = ()=> {
    if(this.hijoModal.nroDocumento && this.hijoModal.fechaNacimiento && this.hijoModal.nombre){
      this.familiares.hijos.push(this.hijoModal);
      this.hijoModal = new Familiar();
      this.agregarHijoModal = false;
    }else{
      showToast(null,'Error', 'Complete los datos solicitados', this.messageService, 'error')
    }
  }

  closeAgregarHijo = ()=> {
    this.hijoModal = new Familiar();
    this.agregarHijoModal = false;
  }

  deleteHijo = (hijo:Familiar)=> {
    let index = 0; 
    this.familiares.hijos.forEach((el, i)=> {
      if( el.nroDocumento === hijo.nroDocumento ){
        index = i;
      }
    });
    this.familiares.hijos.splice(index, 1);
  }

  guardarDatos(){
    this.ngxService.start();
    // this.persona.cantidadHijos = this.familiares.hijos.length;
    // this.persona.familiares = JSON.stringify(this.familiares);
    this.apiPersona.actualizarDatos(this.persona).subscribe(
      data => {
        if(data["estado"] == 0){
          showToast(null,'Éxito', 'Datos guardados correctamente', this.messageService, 'success');
        }
        this.ngxService.stop();
      },
      error => {
        showToast(null,'Error', 'Error al guardar datos', this.messageService, 'error');
      }
    )
  }
}
