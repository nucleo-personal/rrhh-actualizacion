import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Message, MessageService } from 'primeng/api';
import { Domicilio } from 'src/models/domicilio';
import { PersonaActualizacion, PersonaActualizacionModel } from 'src/models/persona';
import { LocalAPI } from 'src/services/local.service';
import { PersonaAPI } from 'src/services/persona.service';
import { checkDomicilio } from 'src/utils/object.checker';
import { showToast } from 'src/utils/toast';

declare var OSMPICKER: any;
declare var $:any;
declare var L:any;
@Component({
  selector: 'app-datos-domicilio',
  templateUrl: './datos-domicilio.component.html',
  styleUrls: ['./datos-domicilio.component.scss']
})
export class DatosDomicilioComponent implements OnInit, OnDestroy {

  personaActualizacion: PersonaActualizacion = new PersonaActualizacion();
  personaModel: PersonaActualizacionModel = new PersonaActualizacionModel();

  constructor(private apiLocal: LocalAPI, private apiPersona: PersonaAPI, private ngxService: NgxUiLoaderService, private messageService: MessageService) {
    apiPersona.personaActualizacion.subscribe(
      data => {
        this.personaActualizacion = data;
        if(this.personaModel.id == null){
          this.personaModel = new PersonaActualizacionModel(this.personaActualizacion);
          try {
            this.domicilio = JSON.parse(this.personaModel.direccion);
            if(this.domicilio == null){
              this.domicilio = new Domicilio();
            }
          }catch(e){
            this.domicilio = new Domicilio();
          }
          this.osm(this.domicilio.lat, this.domicilio.lon);
        }
      }
    )
  }

  domicilio = new Domicilio();
  mensajes: Message[];

  ngOnInit(): void {
    this.mensajes = [
      {severity:'info', summary:'Seleccione su ubicación', detail:'Arrastre el marcador azul a la correspondiente ubicación de su domicilio'}
    ];
    this.getCiudades();
  }

  ngOnDestroy(): void{}

  ciudades = [];
  getCiudades = ()=> {
    this.apiLocal.getCiudades().subscribe(
      data => {
        this.ciudades = data.map( (el, index)=> {
          return {
            ciudad: el.ciudad,
            departamento: el.departamento,
            dropLabel: `${el.ciudad} - ${el.departamento}`
          }
        });
      }
    )
  }

  changeCiudad = () => {
    for(let i=0; i<this.ciudades.length;i++){
      let c = this.ciudades[i];
      if(c.ciudad == this.domicilio.ciudad){
        this.domicilio.departamento = c.departamento;
        break;
      }
    }
    console.log(this.domicilio);
  }

  guardarDatos(){
    if(checkDomicilio(this.domicilio)){
      this.ngxService.start();
      this.personaModel.direccion = JSON.stringify(this.domicilio);
      this.apiPersona.guardarDatos(this.personaModel, this.personaActualizacion.method).subscribe(
        data => {
          if(data["estado"] == 0){
            showToast(null,'Éxito', 'Datos guardados correctamente', this.messageService, 'success');
            this.actualizarDatosService();
          }
          this.ngxService.stop();
        },
        error => {
          showToast(null,'Error', 'Error al guardar datos', this.messageService, 'error');
        }
      )
    }else{
      showToast(null,'Error', 'Complete los campos requeridos', this.messageService, 'error'); 
    }
  }

  nucleoLat = -25.287156078407357;
  nucleoLon = -57.5877712314676;


  map;
  marker;
  osm = (lat, lon) => {
    console.log(this.domicilio);
    lat = lat != null ? lat : this.nucleoLat;
    lon = lon != null ? lon : this.nucleoLon;
    
    try{
      this.map = new L.Map('locationPicker', {});

      let osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
      let osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
      let osm = new L.TileLayer(osmUrl, {minZoom: 8, maxZoom: 18, attribution: osmAttrib});		
      this.map.setView([lat, lon],10);
      this.map.addLayer(osm);

      this.marker = new L.marker([lat, lon]);
      this.map.addLayer(this.marker);


      this.map.on('click', (e) => {        
        let popLocation= e.latlng;
        this.map.removeLayer(this.marker);
        this.marker = new L.marker(popLocation);
        this.map.addLayer(this.marker);
        this.domicilio.lat = popLocation.lat;
        this.domicilio.lon = popLocation.lng; 
        console.log(this.domicilio);
      });
      
      // this.marker.on('dragend', (e) => {
      //   this.map.setView(e.target.getLatLng());
      //   this.domicilio.latitud = e.target.getLatLng().lat;
      //   this.domicilio.longitud = e.target.getLatLng().lng;
      //   console.log(this.domicilio);
      // });
		}catch(e){}
  }

  actualizarDatosService(){
    this.personaActualizacion.direccion = this.personaModel.direccion;

    if(this.personaActualizacion.method == "POST"){
      this.personaActualizacion.method = "PUT";
    }
    this.apiPersona.setPersonaActualizacion(this.personaActualizacion);
  }

  checkNroCasa(e){
    if(this.domicilio.nroCasa <= 0){
      this.domicilio.nroCasa = null;
    }
  }
}
