import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { PagesComponent } from './pages.component';
import { PrimengModule } from 'src/utils/primeng/primeng.module';
import { PagesRoutingModule } from './pages-routing.module';
import { DatosPersonalesComponent } from './datos-personales/datos-personales.component';
import { DatosDomicilioComponent } from './datos-domicilio/datos-domicilio.component';
import { DatosFamiliaresComponent } from './datos-familiares/datos-familiares.component';
import { FormsModule } from '@angular/forms';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { DatosArchivosComponent } from './datos-archivos/datos-archivos.component';
import { EditarDatosComponent } from './editar-datos/editar-datos.component';


@NgModule({
  declarations: [PagesComponent,
    DatosPersonalesComponent,
    DatosDomicilioComponent,
    DatosFamiliaresComponent,
    DatosArchivosComponent,
    EditarDatosComponent],
  imports: [
    CommonModule,
    PrimengModule,
    FormsModule,
    NgxUiLoaderModule,
    PagesRoutingModule
  ],
  providers: [
    DatePipe
  ]
})
export class PagesModule { }
