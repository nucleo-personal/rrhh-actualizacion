import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/services/auth-guard.service';
import { EditarDatosComponent } from './editar-datos/editar-datos.component';
import { PagesComponent } from './pages.component';

const routes: Routes = [
  {
    path: "",
    component: PagesComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: "",
        redirectTo: "mis-datos",
        pathMatch: "full"
      },
      {
        path: "mis-datos",
        component: EditarDatosComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
