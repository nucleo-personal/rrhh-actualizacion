import { Component, OnInit } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Message, MessageService } from 'primeng/api';
import { Archivo } from 'src/models/file';
import { PersonaAPI } from 'src/services/persona.service';
import { fileHandler } from 'src/utils/file-handler'
import { showToast } from 'src/utils/toast';
import { PersonaActualizacion, PersonaActualizacionModel } from 'src/models/persona';

@Component({
  selector: 'app-datos-archivos',
  templateUrl: './datos-archivos.component.html',
  styleUrls: ['./datos-archivos.component.scss']
})
export class DatosArchivosComponent implements OnInit {

  personaActualizacion: PersonaActualizacion = new PersonaActualizacion();
  personaModel: PersonaActualizacionModel = new PersonaActualizacionModel();

  constructor(private messageService: MessageService, private ngxService: NgxUiLoaderService, private apiPersona: PersonaAPI) {
    apiPersona.personaActualizacion.subscribe(
      data => {
        this.personaActualizacion = data;
        this.personaModel = new PersonaActualizacionModel(this.personaActualizacion);
        
        this.archivos = [];
        
        if(this.personaModel.fotoCedulaFrontal != null){
          let exff = this.personaModel.fotoCedulaFrontal.toString().indexOf("png") !== -1 ? "png":"jpg";
          let archivoff = new Archivo();
          archivoff.nombre = `cedula-frontal.${exff}`;
          archivoff.file = this.personaModel.fotoCedulaFrontal;
          archivoff.tipo = "CI FRONTAL";
          this.archivos.push(archivoff);
        }
        
        if(this.personaModel.fotoCedulaDorsal != null){
          let exfd = this.personaModel.fotoCedulaDorsal.toString().indexOf("png") !== -1 ? "png":"jpg";
          let archivofd = new Archivo();
          archivofd.nombre = `cedula-dorsal.${exfd}`;
          archivofd.file = this.personaModel.fotoCedulaDorsal;
          archivofd.tipo = "CI DORSAL";
          this.archivos.push(archivofd);
        }
        
      }
    )
  }

  
  archivos: Archivo[] = [];
  inputFrontal;
  inputDorsal;
  mensajes: Message[];

  ngOnInit(): void {
    this.mensajes = [
      {severity:'info', summary:'Adjunte imágenes de su documento de identidad, frontal y dorsal', detail:'Formatos permitidos PNG, JPEG, JPG. Tamaño máximo 2MB.'}
    ];
    this.inputFrontal = <HTMLInputElement> document.getElementById('file-selector-f');
    this.inputDorsal = <HTMLInputElement> document.getElementById('file-selector-d');
  }

  openFileSelectorFrontal(){
    this.inputFrontal.click();
  }
  
  openFileSelectorDorsal(){
    this.inputDorsal.click();
  }

  onChangeFileSelectorFrontal = (files:FileList) => {
    fileHandler(files).then((archivo:Archivo) => {
      if(archivo != null){
        let index=null;
        for(let i=0; i<this.archivos.length;i++){
          if(this.archivos[i].tipo == "CI FRONTAL"){
            index = i;
            break;
          }
        }
        if(index != null){
          this.archivos.splice(index, 1);
        }
        let nombreOriginal = archivo.nombre;
        archivo.tipo = "CI FRONTAL";
        archivo.nombre = `cedula-frontal.${nombreOriginal.split('.').pop()}`;
        this.archivos.push(archivo);
      }else{
        showToast(null,'Error en el archivo', 'Formato inválido o tamaño excedido', this.messageService, 'error');
      }
      this.inputFrontal.value = "";
    });
  }
  
  
  onChangeFileSelectorDorsal = (files:FileList) => {
    fileHandler(files).then((archivo:Archivo) => {
      if(archivo != null){
        let index=null;
        for(let i=0; i<this.archivos.length;i++){
          if(this.archivos[i].tipo == "CI DORSAL"){
            index = i;
            break;
          }
        }
        if(index != null){
          this.archivos.splice(index, 1);
        }
        let nombreOriginal = archivo.nombre;
        archivo.tipo = "CI DORSAL";
        archivo.nombre = `cedula-dorsal.${nombreOriginal.split('.').pop()}`;
        this.archivos.push(archivo);
      }else{
        showToast(null,'Error en el archivo', 'Formato inválido o tamaño excedido', this.messageService, 'error');
      }
      this.inputDorsal.value = "";
    });
  }

  descargarArchivo = (target: Archivo) => {
    let a = document.createElement("a");
    a.download = target.nombre;
    a.href = target.file;
    a.click();
  }

  deleteArchivo = (archivo:Archivo)=> {
    let index = 0; 
    this.archivos.forEach((el, i)=> {
      if( el.nombre === archivo.nombre ){
        index = i;
      }
    });
    this.archivos.splice(index, 1);
  }

  guardarDatos(){
    let hasff = false;
    let hasfd = false;

    for(let i=0; i<this.archivos.length;i++){
      if(this.archivos[i].tipo == "CI DORSAL"){
        this.personaModel.fotoCedulaDorsal = this.archivos[i].file;
        hasfd = true;
      }else if(this.archivos[i].tipo == "CI FRONTAL"){
        this.personaModel.fotoCedulaFrontal = this.archivos[i].file;
        hasff = true;
      }
    }

    if(hasff && hasfd){
      this.ngxService.start();
      this.apiPersona.guardarDatos(this.personaModel, this.personaActualizacion.method).subscribe(
        data => {
          if(data["estado"] == 0){
            showToast(null,'Éxito', 'Datos guardados correctamente', this.messageService, 'success');
            this.actualizarDatosService();
          }
          this.ngxService.stop();
        },
        error => {
          showToast(null,'Error', 'Error al guardar datos', this.messageService, 'error');
        }
      )
    }else{
      showToast(null,'Error', 'Favor cargue las fotos de su CI para guardar', this.messageService, 'error');
    }
  }

  actualizarDatosService(){
    this.personaActualizacion.fotoCedulaFrontal = this.personaModel.fotoCedulaFrontal;
    this.personaActualizacion.fotoCedulaDorsal = this.personaModel.fotoCedulaDorsal;
    if(this.personaActualizacion.method == "POST"){
      this.personaActualizacion.method = "PUT";
    }
    this.apiPersona.setPersonaActualizacion(this.personaActualizacion);
  }

  verFoto = false;
  targetFoto = null;
  showFoto(target:Archivo){
    this.targetFoto = target.file;
    this.verFoto = true;
  }
}
