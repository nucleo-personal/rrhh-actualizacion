import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { PersonaActualizacion } from 'src/models/persona';
import { AuthAPI } from 'src/services/auth.service';
import { PersonaAPI } from 'src/services/persona.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private apiAuth: AuthAPI, private router: Router, private messageService: MessageService, private apiPersona: PersonaAPI) { }

  loading = false;
  key = {
    usuario: null,
    clave: null
  }

  ngOnInit(): void {}

  login = async() => {
    if(this.key.usuario != null && this.key.usuario != "" && this.key.clave != null && this.key.clave != ""){
      this.loading = true;
      let loginResult = await this.apiAuth.login(this.key).toPromise().catch(err => { this.showError("Usuario y/o contraseña incorrecta/s"); this.loading=false;});
      if(loginResult?.estado == 0){
        localStorage.setItem("token", loginResult?.dato["X-Auth-Token"]);
        localStorage.setItem("sesion", "S");
        localStorage.setItem("currentUser", JSON.stringify(loginResult?.dato[0]));
        // localStorage.setItem("usuario", this.key.usuario);
        // let p: PersonaActualizacion = loginResult?.dato["datos_usuario"];
        // this.apiPersona.setPersonaActualizacion(p);
        this.router.navigate([""]);
      }
    }else{
      this.showError("Complete los campos requeridos");
    }
    
  }

  showError(mensaje) {
    this.messageService.add({severity:'error', summary: 'Error', detail: mensaje});
  }
}
