export class Domicilio {
    callePrincipal: string = null;
    calleSecundaria: string = null;
    nroCasa: number = null;
    ebp: string = null;
    barrio: string = null;
    ciudad: string = null;
    departamento: string = null;
    lat: string = null;
    lon: string = null;
}