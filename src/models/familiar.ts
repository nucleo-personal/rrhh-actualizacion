export class Familiar {
    nombre: string;
    nroDocumento: string;
    fechaNacimiento: string;
}

export class EstructuraFamiliar {
    conyuge: Familiar = new Familiar();
    hijos: Familiar[] = [];
}