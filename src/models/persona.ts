export class Persona {
    id: number;
    nombres: string;
    apellidos: string;
    documento: string;
    sexo: string;
    direccion: any;
    fechaNacimiento: string;
    email: string;
    telefono: string;
    foto: any;
    nacionalidad: string;
    estadoCivil: string;
    celular: string;
    usuario: string;
    superior: string;
    legajoCelular: string;
    legajoTelefonoInterno: string;
    persona: number;
    fotoCedulaFrontal: any;
    fotoCedulaDorsal: any;
    conyuge: any;
    hijos: any;

    constructor(obj?: ViewPersona){
        this.id= obj && obj.id || null;
        this.nombres= obj && obj.personaNombres || null;
        this.apellidos= obj && obj.personaApellidos || null;
        this.documento= obj && obj.personaDocumento || null;
        this.sexo= obj && obj.sexo || null;
        this.direccion= obj && obj.direccion || null;
        this.fechaNacimiento= obj && obj.fechaNacimiento || null;
        this.email= obj && obj.email || null;
        this.telefono= obj && obj.telefono || null;
        this.foto = obj && obj.foto|| null;
        this.nacionalidad= obj && obj.nacionalidad || null;
        this.estadoCivil= obj && obj.estadoCivil || null;
        this.celular= obj && obj.celular || null;
        this.usuario = obj && obj.usuario || null;
        this.superior = obj && obj.superior || null;
        this.legajoCelular = obj && obj.legajoCelular || null;
        this.legajoTelefonoInterno = obj && obj.telefonoInterno || null;
        this.persona = obj && obj.persona || null;
        this.fotoCedulaDorsal = obj && obj.fotoCedulaDorsal || null;
        this.fotoCedulaFrontal = obj && obj.fotoCedulaFrontal || null;
        this.conyuge = obj && obj.conyuge || null;
        this.hijos = obj && obj.hijos || null;
    }
}

export class ViewPersona {
    method: string = null;
    persona: number = null;
    id: number = null;
    personaNombres: string = null;
    personaApellidos: string = null;
    personaDocumento: string = null;
    sexo: string = null;
    direccion: any = null;
    fechaNacimiento: string =null;
    email: string = null;
    telefono: string = null;
    foto: string = null;
    nacionalidad: string = null;
    estadoCivil: string = null;
    celular: string = null;
    legajoCelular: string = null;
    telefonoInterno: string = null;
    superior: string = null;
    fotoCedulaFrontal: string = null;
    fotoCedulaDorsal: string = null;
    hijos: any = null;
    conyuge: any = null;
    usuario: string = null;
    emailLaboral: string = null;
    tipoEmpleadoNombre: string = null;
    cargo: string = null;
    nivelNumbero: string = null;
}

export class PersonaActualizacion {
    cargo: string = null;
    celular: string = null;
    celularAD: string = null;
    conyuge: any = null;
    direccion: any = null;
    email: string = null;
    emailLaboral: string = null;
    estadoCivil: string = null;
    fechaNacimiento: string = null;
    foto: any = null;
    fotoCedulaDorsal: any = null;
    fotoCedulaFrontal: any = null;
    hijos = null;
    id: number = null;
    legajoCelular: string = null;
    method: string = null;
    nacionalidad: string = null;
    nivelNumero: number = null;
    oficina: number = null;
    oficinaNombre: string = null;
    oficinaRegion: string = null;
    persona: number = null;
    personaApellidos: string = null;
    personaNombres: string = null;
    personaDocumento: string = null;
    sexo: string = null;
    superior: string = null;
    telefono: string = null;
    telefonoInterno: string = null;
    tipoEmpleadoNombre: string = null;
    usuario: string = null;
}

export class PersonaActualizacionModel {
    id: number = null;
    apellidos: string = null;
    nombres: string = null;
    documento: string = null;
    persona: number = null;
    sexo: string = null;
    direccion: any = null;
    fechaNacimiento: string = null;
    email: string = null;
    telefono: string = null;
    foto: any = null;
    fotoCedulaDorsal: any = null;
    fotoCedulaFrontal: any = null;
    nacionalidad: string = null;
    estadoCivil: string = null;
    celular: string = null;
    superior: string = null;
    usuario: string = null;
    legajoCelular: string = null;
    legajoTelefonoInterno: string = null;
    conyuge: any = null;
    hijos = null;
    oficina: number = null;

    constructor(obj?: PersonaActualizacion){
        this.id= obj && obj.id || null;
        this.nombres= obj && obj.personaNombres || null;
        this.apellidos= obj && obj.personaApellidos || null;
        this.documento= obj && obj.personaDocumento || null;
        this.sexo= obj && obj.sexo || null;
        this.direccion= obj && obj.direccion || null;
        this.fechaNacimiento= obj && obj.fechaNacimiento || null;
        this.email= obj && obj.email || null;
        this.telefono= obj && obj.telefono || null;
        this.foto = obj && obj.foto|| null;
        this.nacionalidad= obj && obj.nacionalidad || null;
        this.estadoCivil= obj && obj.estadoCivil || null;
        this.usuario = obj && obj.usuario || null;
        this.superior = obj && obj.superior || null;
        this.legajoCelular = obj && obj.legajoCelular || null;
        this.legajoTelefonoInterno = obj && obj.telefonoInterno || null;
        this.persona = obj && obj.persona || null;
        this.fotoCedulaDorsal = obj && obj.fotoCedulaDorsal || null;
        this.fotoCedulaFrontal = obj && obj.fotoCedulaFrontal || null;
        this.oficina = obj && obj.oficina || null;
    }
}