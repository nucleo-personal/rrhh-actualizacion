export const showToast = (toastKey ,header, mensaje, mensajeService, type) => {
    
    const key = "mensajes";
    
    mensajeService.add({key: toastKey != null? toastKey:key ,severity:type, summary: header, detail: mensaje});
}