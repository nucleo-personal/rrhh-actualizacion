import { Domicilio } from "src/models/domicilio";
import { Familiar } from "src/models/familiar";
import { PersonaActualizacionModel } from "src/models/persona"
export const checkPersona = (obj: PersonaActualizacionModel, requiredAttr: string[]) => {
    const requiredAttrDefault = ["documento", "nombres", "apellidos", "sexo", "estadoCivil", "nacionalidad", "fechaNacimiento", "email", "oficina"];
    let ok = true;
    let attributes = requiredAttr != null? requiredAttr: requiredAttrDefault;
    for(let i=0; i<attributes.length; i++){
        let attribute = attributes[i];
        if(obj[attribute] === null || obj[attribute].toString() === "" || obj[attribute].toString().replaceAll(/\s/g,'') === ""){
            ok = false;
            console.log(obj[attribute], attribute);
            break;
        }
    }
    if (!validarEmail(obj.email)) {
        return false;
    }
    return ok;
}

export const validarEmail = (email) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/;
    return re.test(String(email).toLowerCase());
}

export const checkDomicilio = (obj: Domicilio) => {
    const requiredAttrDefault = ["callePrincipal", "barrio", "ciudad", "departamento"];
    let ok = true;
    let attributes = requiredAttrDefault;
    for(let i=0; i<attributes.length; i++){
        let attribute = attributes[i];
        if(obj[attribute] === null || obj[attribute].toString() === "" || obj[attribute].toString().replaceAll(/\s/g,'') === ""){
            ok = false;
            break;
        }
    }
    return ok;
}

export const checkFamiliar = (obj: Familiar) => {
    const requiredAttrDefault = ["documento", "nombre", "fechaNacimiento"];
    let ok = true;
    let attributes = requiredAttrDefault;
    for(let i=0; i<attributes.length; i++){
        let attribute = attributes[i];
        if(obj[attribute] === null || obj[attribute].toString() === "" || obj[attribute].toString().replaceAll(/\s/g,'') === ""){
            ok = false;
            break;
        }
    }
    return ok;
}