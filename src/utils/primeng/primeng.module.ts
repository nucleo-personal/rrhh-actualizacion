import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MenubarModule} from 'primeng/menubar';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {BadgeModule} from 'primeng/badge';
import {CardModule} from 'primeng/card';
import { MenuModule } from 'primeng/menu';
import { FieldsetModule, } from 'primeng/fieldset';
import {DialogModule} from 'primeng/dialog';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {DropdownModule} from 'primeng/dropdown';
import {SplitterModule} from 'primeng/splitter';
import {DividerModule} from 'primeng/divider';
import {TableModule} from 'primeng/table';
import {ToastModule} from 'primeng/toast';
import {ScrollTopModule} from 'primeng/scrolltop';
import {InputMaskModule} from 'primeng/inputmask';
import {SidebarModule} from 'primeng/sidebar';
import {AvatarModule} from 'primeng/avatar';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports: [
    MenubarModule,
    MenuModule,
    InputTextModule,
    ButtonModule, 
    BadgeModule,
    CardModule,
    FieldsetModule,
    DialogModule,
    MessagesModule,
    MessageModule,
    DropdownModule,
    SplitterModule,
    DividerModule,
    TableModule,
    ToastModule,
    ScrollTopModule,
    InputMaskModule,
    SidebarModule,
    AvatarModule
  ]
})
export class PrimengModule { }
