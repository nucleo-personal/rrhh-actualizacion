import { Archivo } from "src/models/file";

export const fileHandler = async(files: FileList) =>  {
    let archivo = new Archivo();
    if (files != null) {
      let file = files.item(0);
      let file64 = await toBase64(file);
      archivo.file = file64.toString();
      archivo.nombre = file.name;
      
      let fileSizeKB = (file.size / 1024);
      if(fileSizeKB <= 1024) {
        archivo.size = fileSizeKB <= 1024? `${fileSizeKB.toFixed(2)}KB`: `${(fileSizeKB/1024).toFixed(2)}MB`;
      }else {
        return null;
      }

      let pattern = /image-*/;
      if (!file.type.match(pattern)) {
        return null;
      }
      
      return archivo;
    }
    return null;
}

const toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});