import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Host } from 'src/utils/host';

@Injectable({
  providedIn: 'root'
})
export class AuthAPI {

  constructor(private http: HttpClient) { }

  login(body):Observable<any>{    
    return this.http.post(`${Host}/crear/_login`, body);
  }

  logout():Observable<any>{
    localStorage.removeItem('sesion');
    localStorage.removeItem('currentUser');    
    return this.http.get(`${Host}/listar/_logout`);
  }

  getUser():Observable<any>{    
    return this.http.get(`${Host}/listar/_user`);
  }

}