import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LocalAPI {

  constructor(private http: HttpClient) { }

  getCiudades():Observable<any>{    
    return this.http.get(`assets/json/ciudades.json`);
  }
  
  getNacionalidades():Observable<any>{    
    return this.http.get(`assets/json/nacionalidades.json`);
  }

}