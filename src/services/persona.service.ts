import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Host } from 'src/utils/host';
import { Subject } from 'rxjs';
import { PersonaActualizacion } from 'src/models/persona';

@Injectable({
  providedIn: 'root'
})
export class PersonaAPI {

  constructor(private http: HttpClient) { }

  private personaActualizacionSource = new Subject<PersonaActualizacion>();
  personaActualizacion = this.personaActualizacionSource.asObservable();

  setPersonaActualizacion(p: PersonaActualizacion) {
    this.personaActualizacionSource.next(p);
  }
  
  getPersona():Observable<any>{   
    return this.http.get(`${Host}/listar/usuario`);
  }

  getPersonaActualizacion(usuario):Observable<any>{    
    return this.http.get(`${Host}/persona_rrhh_actualizacion/usuario/${usuario}`);
  }
  
  actualizarDatos(body){
    if(localStorage.getItem('method') == 'POST'){
      localStorage.setItem('method', 'PUT');
      return this.http.post(`${Host}/persona_rrhh_actualizacion/crear`, body);
    }else{
      return this.http.put(`${Host}/persona_rrhh_actualizacion/modificar/${body.id}`, body);      
    }
  }

  guardarDatos(body, method){
    if(method == "POST"){
      return this.http.post(`${Host}/persona_rrhh_actualizacion/crear`, body);
    }else{
      return this.http.put(`${Host}/persona_rrhh_actualizacion/modificar/${body.id}`, body);      
    }
  }

  getNotificacion(persona):Observable<any>{    
    return this.http.get(`${Host}/notificacion_rrhh_actualizacion/persona/${persona}`);
  }
  
  getOficinas():Observable<any>{    
    return this.http.get(`${Host}/listar/oficina_rrhh`);
  }
}